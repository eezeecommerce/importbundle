<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 22/03/16
 * Time: 15:33
 */

namespace eezeecommerce\ImportBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\ProductBundle\Entity\Product;
use eezeecommerce\WebBundle\Entity\Uri;

class LoadProductData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        if (($handle = fopen(__DIR__ . "/../../Resources/data/products.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 7000, ",")) !== FALSE) {
                list($id, $sku ,$productname, $longDescription, $shortDescription, $price, $tradeUkPrice, $tradeUsPrice, $tradeEuPrice, $retailUkPrice, $retailUsaPrice, $retailEuPrice, $unitWeight, $unitLength, $unitHeight, $unitDepth, $uri, $metaDescription, $metaKeywords, $metaTitle, $textEngraving, $logoEngraving, $image, $oldUrl, $freeTextEngraving, $boxPrinting, $allowExpress) = $data;
                $url = new Uri();
                $url->setDescription($metaDescription);
                $url->setKeywords($metaKeywords);
                $url->setTitle($metaTitle);
                $url->setSlug($uri);
                $manager->persist($url);

                $product = new Product();
                $product->setBasePrice($price);
                $product->setDepth($unitDepth);
                $product->setHeight($unitHeight);
                $product->setLength($unitLength);
                $product->setWeight($unitWeight);
                $product->setLocale("en");
                $product->setLongDescription($longDescription);
                $product->setShortDescription($shortDescription);
                $product->setStockCode($productname);
                $product->setProductName($productname);
                $product->setSku($sku);
                $product->setUri($url);

                $manager->persist($product);

            }
            fclose($handle);
        } else {
            throw new RuntimeException('Failed to parse Products');
        }
        $manager->flush();
    }
}