<?php

namespace eezeecommerce\ImportBundle\Admin;

use eezeecommerce\ProductBundle\Entity\Product;
use eezeecommerce\StockBundle\Entity\Stock;
use eezeecommerce\WebBundle\Entity\Uri;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Created by PhpStorm.
 * User: liam
 * Date: 12/01/2017
 * Time: 17:37
 */
class ImportController extends Controller
{
    /**
     * @Route("/admin/products/export/csv", name="_eezeecommerce_admin_products_csv_export")
     */
    public function csvDownloadAction()
    {
        $results = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")->findAll();

        $response = new StreamedResponse();
        $response->setCallback(function() use ($results){
            $handle = fopen('php://output', 'w+');

            fputcsv($handle, array("id", "product_name", "long_description", "short_description", "sku", "stock_code",
                "turnaround_min_override", "turnaround_max_override", "bay_number", "base_price",
                "length", "height", "depth", "disabled", "weight", "stock", "was", "was_price", "uri_title", "uri_url",
                "uri_keywords", "uri_description"), ',');



            foreach ($results as $product) {
                fputcsv(
                    $handle,
                    array(
                        $product->getId(), $product->getProductName(), $product->getLongDescription(), $product->getShortDescription(), $product->getSku(),
                        $product->getStockCode(), $product->getTurnaroundMinOverride(), $product->getTurnaroundMaxOverride(), $product->getBayNumber(), $product->getBasePrice(),
                        $product->getLength(), $product->getHeight(), $product->getDepth(), $product->getDisabled(), $product->getWeight(), $product->getStock()->getCurrentStock(), $product->getWas(), $product->getWasPrice(),
                        $product->getUri()->getTitle(), $product->getUri()->getUrl(), $product->getUri()->getKeywords(), $product->getUri()->getDescription()
                    ), ','
                );
            }
            fclose($handle);
        });
        $response->setStatusCode(200);
        $response->headers->set("Content-Type", "text/csv; charset=utf-8");
        $response->headers->set("Content-Disposition", 'attachment; filename="product_export.csv"');

        return $response;
    }

    /**
     * @Route("/admin/products/import/csv", name="_eezeecommerce_admin_products_csv_import")
     * @Method({"POST"})
     */
    public function csvUploadAction(Request $request)
    {
        $files = $request->files;

        if (count($files) < 1) {
            $this->addFlash("danger", "No file was uploaded. Please choose a file and try again");

            return $this->redirect($this->generateUrl("_eezeecommerce_admin_products_index"));
        }

        if (!$files->has("csvFile")) {

            $this->addFlash("danger", "There was a problem submitting the form. Please try again or contact support");

            return $this->redirect($this->generateUrl("_eezeecommerce_admin_products_index"));
        }

        $file = $files->get("csvFile");

        if ($file->guessExtension() != "csv" && $file->guessExtension() != "txt" ) {
            $this->addFlash("danger", "The file uploaded is not a csv. Please save your file as a csv and try again");

            return $this->redirect($this->generateUrl("_eezeecommerce_admin_products_index"));
        }

        ini_set('auto_detect_line_endings',TRUE);
        $handle = fopen($file->getRealPath(), "r");
        $errors = array();
        $em = $this->getDoctrine()->getEntityManager();
        $i = 1;
        while (($data = fgetcsv($handle)) !== FALSE) {
            list($id, $product_name, $long_description, $short_description, $sku, $stock_code, $turnaround_min_override, $turnaround_max_override, $bay_number, $base_price,
                $length, $height, $depth, $disabled, $weight, $stock, $was, $was_price, $uri_title, $uri_url, $uri_keywords, $uri_description
                ) = $data;
            if ($i == 1) {
                $i++;
                continue;
            }

            if (null === $id || empty($id)) {
                try {
                    $em->getConnection()->beginTransaction();
                    $product = new Product();
                    $product->setProductName($product_name);
                    $product->setLongDescription($long_description);
                    $product->setShortDescription($short_description);
                    $product->setLocale("en");
                    $product->setStockCode($stock_code);
                    $product->setTurnaroundMinOverride($turnaround_min_override);
                    $product->setTurnaroundMaxOverride($turnaround_max_override);
                    $product->setBayNumber($bay_number);
                    $product->setLength($length);
                    $product->setHeight($height);
                    $product->setDepth($depth);
                    $product->setDisabled($disabled);
                    $product->setWeight($weight);
                    $stockEntity = new Stock();
                    $stockEntity->setCurrentStock($stock);
                    $product->setStock($stockEntity);
                    $product->setWas($was);
                    $product->setWasPrice($was_price);

                    $uri = new Uri();
                    $uri->setTitle($uri_title);
                    $uri->setUrl($uri_url);
                    $uri->setKeywords($uri_keywords);
                    $uri->setDescription($uri_description);
                    $product->setBasePrice($base_price);
                    $product->setUri($uri);
                    $em->persist($stockEntity);
                    $em->persist($uri);
                    $em->persist($product);
                    $em->flush();
                    $em->getConnection()->commit();
                } catch(\Exception $e) {
                    $em->rollback();
                    $errors[] = $sku;
                }
            } else {
                try {

                    $em->getConnection()->beginTransaction();
                    $product = $em->getRepository("eezeecommerceProductBundle:Product")->find($id);
                    $product->setProductName($product_name);
                    $product->setLongDescription($long_description);
                    $product->setShortDescription($short_description);
                    $product->setLocale("en");
                    $product->setStockCode($stock_code);
                    $product->setTurnaroundMinOverride($turnaround_min_override);
                    $product->setTurnaroundMaxOverride($turnaround_max_override);
                    $product->setBayNumber($bay_number);
                    $product->setLength($length);
                    $product->setHeight($height);
                    $product->setDepth($depth);
                    $product->setSku($sku);
                    $product->setDisabled($disabled);
                    $product->setWeight($weight);
                    if (null === $product->getStock()) {
                        $s = new Stock();
                        $s->setCurrentStock($stock);
                    } else {
                        $s = $product->getStock()->setCurrentStock($stock);
                    }
                    $product->getStock($s);
                    $product->setWas($was);
                    $product->setWasPrice($was_price);

                    $uri = $product->getUri();
                    $uri->setTitle($uri_title);
                    $uri->setUrl($uri_url);
                    $uri->setKeywords($uri_keywords);
                    $uri->setDescription($uri_description);

                    $product->setBasePrice($base_price);
                    $em->persist($s);
                    $em->persist($product);
                    $em->persist($uri);
                    $em->flush();
                    $em->getConnection()->commit();
                } catch(\Exception $e) {
                    $em->rollback();
                    $errors[] = $sku;
                }
            }
            $i++;
        }
        ini_set('auto_detect_line_endings',FALSE);

        if (count($errors) > 0) {
            $this->addFlash("danger", "Unfortunately, we were unable to upload the following product sku's due to a conflict. Please try again or contact support: ".implode(" ,",$errors));
        }else {
            $this->addFlash("success", "Your upload was successful");
        }

        return $this->redirect($this->generateUrl("_eezeecommerce_admin_products_index"));

    }
}